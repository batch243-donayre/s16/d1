
// [section] Arithmetic Operators
let x = 1397;
console.log("The value of x is :" +x);
let y = 7831;
console.log("The value of y is :" +y);

let sum = x+y;
console.log("Result of addition operator: " +sum );

// Difference operator
let difference = x - y;
console.log("Result of Subtraction operator: " +difference );

// Multiplication Operator
let	product = x * y;
console.log("Result of multiplication operator: " +product );

// Multiplication Operator
let	quotient = x / y;
console.log("Result of division operator: " +quotient );

// Modulo
let remainder = y % x;
console.log("Result of modulo: " +remainder );

let secondremainder = x % y;
console.log("Result of modulo: " +secondremainder );

// [section] Assignment operators
	// Basic assignment operator adds the value of the right operand to a variable and assigns the result to the value.

let assignmentNumber=8;
console.log(assignmentNumber);

// Addition assignment operator adds the value of the right operand to  a variable and assigns the result to the variable.
// assignmentNumber = assignmentNumber+2;
// console.log(assignmentNumber);

assignmentNumber +=2;
console.log("Result of adition assignment operator : " +assignmentNumber);

// Subtractionassignment operator (-=)
assignmentNumber -=2;
console.log("Result of subtraction assignment operator : " +assignmentNumber);

// Multiplication Assignment operator (*=)
assignmentNumber *=4;
console.log("Result of multiplication assignment operator : " +assignmentNumber);

// Division Assignment operator (/=)
assignmentNumber /=8;
console.log("Result of division assignment operator : " +assignmentNumber);

// Multiple Operator and Parenthesis
/*
	1. 3*4=12
	2. 12/5 = 2.4
	3. 1+2 =3
	4. 3-2.4=0.6
*/
let mdas = 1 + 2 - 3 * 4/5;
console.log("Result of mdas rule : " +mdas);

let	pemdas = 1 + (2 - 3) * (4/5);
console.log("Result of pemdas rule : " +pemdas);

// [section] Incrementation and Decrement 
// Operators that add or subtract values by 1 and reaasign the value of the variable where the increment/decrement was applied to

let z = 1;

// Pre-increment (Automatic add 1)
let increment = ++z;
console.log("Result of z in pre-increment : "+z);
console.log("Result of increment in pre-increment : "+increment);

//Post-increment (Add after declaration)
increment = z++;
console.log("Result of z in post-increment : "+z);
console.log("Result of increment in post-increment : "+increment);
increment = z++;
console.log("Result of increment in post-increment : "+increment);


let	p = 0;
// pre-decrement

let decrement = --p;
console.log("Result of z in pre-increment : " +decrement);
console.log("Result of decrement in pre-increment : " +decrement);

// post-decrement
decrement = p--;
console.log("Result of p in post-decrement : " +decrement);
console.log("Result of decrement in post-decrement : "+decrement);

// [section] Type Coercion

/*
	-type coercion
*/
 let numA = '10';
 let numB = 12;

 /*
	-adding or concatinating a string and a number witll result into string.
	-This can be proven in the console by at the color of the text display
 */

 let coercion =numA + numB;
 console.log(coercion);
 console.log(typeof coercion);


 // Non-coersion
 let numC = 16;
 let numD = 14;	
 let noncercion = numC + numD;
 console.log(noncercion);
 console.log(typeof noncercion);

 let numE = true + 1;
 console.log(numE);

 // false = 0, true =1
 // let numE = false + 1;
 // console.log(numE);

 /*
	Type Coercion: Boolean value and number
	The result will be a nuber
	The Boolean value will be converted. true-1, false=0
 */

 // [section] Comparison operators

 let juan = 'juan';

 	// Equality Operator (==)
	/*
		checl wether the operands are equal have the same value
	*/

let	isEqual =1 ==1;
 console.log(typeof isEqual);

 console.log (1 == 2);
 console.log (1 == '1');

 // Strict equality operator (===)
 console.log(1 === '1');
 console.log('juan' == 'juan');
 console.log('juan' == juan);

 // Inequality Operator (!=)
 /*
	=checks wether the operands are not equal/have different content 
	-attemps to convert or compare operands of defferent data types
 */

 console.log(1 != 1);
 console.log(1 != 2);
 console.log(1 != '1');


 // Strict Inqequality Operator
 console.log(1 !== '1');


 // [section] Relational Operators

 // somecomparison operators check whether one value is greater or les than to other value.

let a = 50;
let b =65;
// GT of greater than operator (>)
let isGreaterThan = a>b;
console.log(isGreaterThan);

 // Lt or Less Than Operator (<)
let isLessThan = a<b;
console.log(isLessThan);

// GTE or greater than or Equal Operator (>=)
let isGTorEqual = a >= b;
console.log(isGTorEqual);

let numStr = "30";
console.log(a>numStr);

console.log(b <= numStr);

// [section] Logical Operators
let isLegalAge = true;
let isRegistered = false;

// Logical And Operator (&& - double ampersand)
// if meron false matic false na
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of Logical Operator : " +allRequirementsMet);

// Logical or Operator (|| - double pipe)

let someRequirementsMet = isLegalAge || isRegistered;
console.log ("Results Logical or Operator: " +someRequirementsMet);

// Logical NootOperator (!- exclamationpoint)
let someRequirementsNotMet	= !isRegistered;
console.log (someRequirementsNotMet);




